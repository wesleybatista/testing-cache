from datetime import datetime


def main():
    with open('.cache/appending_file.txt', 'a+') as f:
        f.write(f'hello human! now is {datetime.utcnow()}\n')

    with open('.cache/appending_file.txt') as f:
        print(f.read())


if __name__ == '__main__':
    main()
